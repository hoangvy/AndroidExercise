package activity.test.hoangvypfiev.eventtest1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button bt_add = (Button) findViewById(R.id.bt_add);
        // Cach 2
//        bt_add.setOnClickListener( new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                EditText tf_number1 = (EditText) findViewById(R.id.tf_number1);
//                int number1 = Integer.parseInt(tf_number1.getText() + "");
//
//                EditText tf_number2 = (EditText) findViewById(R.id.tf_number2);
//                int number2 = Integer.parseInt(tf_number2.getText() + "");
//
//                int result = number1 + number2;
//                TextView tx_result = (TextView) findViewById(R.id.tx_result);
//                tx_result.setText( Integer.toString(result) );
//            }
//        });
        bt_add.setOnClickListener(this);
    }

    // Cach 3
    @Override
    public void onClick(View view) {
        EditText tf_number1 = (EditText) findViewById(R.id.tf_number1);
                int number1 = Integer.parseInt(tf_number1.getText() + "");

                EditText tf_number2 = (EditText) findViewById(R.id.tf_number2);
                int number2 = Integer.parseInt(tf_number2.getText() + "");

                int result = number1 + number2;
                TextView tx_result = (TextView) findViewById(R.id.tx_result);
                tx_result.setText( Integer.toString(result) );
    }
    /* Cach 1
    public int addNumber( View view)
    {
        EditText tf_number1 = (EditText) findViewById(R.id.tf_number1);
        int number1 = Integer.parseInt(tf_number1.getText() + "");

        EditText tf_number2 = (EditText) findViewById(R.id.tf_number2);
        int number2 = Integer.parseInt(tf_number2.getText() + "");

        int result = number1 + number2;
        TextView tx_result = (TextView) findViewById(R.id.tx_result);
        tx_result.setText( Integer.toString(result) );
        return result;
    }
    */
}
